const setProducts = (context, payload) => {
  context.commit('SET_PRODUCTS', payload)
}

const setKeyword = (context, payload) => {
  context.commit('SET_KEYWORD', payload)
}

export default {
  setProducts,
  setKeyword
}
