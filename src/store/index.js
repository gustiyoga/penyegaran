import Vue from 'vue'
import Vuex from 'vuex'

import mutations from './mutations'
import actions from './actions'
import getters from './getters'

Vue.use(Vuex)

const state = () => {
  return {
    API_URL: '/api/',
    products: [],
    keyword: ''
  }
}

export const store = new Vuex.Store({
  modules: {
    // some module
  },
  state,
  mutations,
  actions,
  getters
})
