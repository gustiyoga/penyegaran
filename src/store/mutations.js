const SET_PRODUCTS = (state, payload) => {
  state.products = payload
}

const SET_KEYWORD = (state, payload) => {
  state.keyword = payload
}

export default {
  SET_PRODUCTS,
  SET_KEYWORD
}
